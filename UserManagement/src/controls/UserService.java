package controls;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.gson.JsonArray;

import dao.SearchDao;

@Path("/UserService")
public class UserService {

   SearchDao searchDao = new SearchDao();
   
   @GET
   @Path("/search")
   @Produces("application/json")
   public String search(@QueryParam("title") String title, @QueryParam("type") String type, @QueryParam("page") int page){
	
	   JsonArray results = new JsonArray();
	   
	   String[] types = type.split(",");
	   
	   for(String t : types){
		   results.addAll(searchDao.search(title, t, page));
	   }
	   
	   return results.toString();
   
   }
}