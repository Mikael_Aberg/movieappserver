package dao;

import java.sql.Connection;

import com.google.gson.JsonArray;

public class SearchDao {

	public static final String MOVIE = "movie";
	public static final String SERIES = "series";
	public static final String GAME = "game";
	
	
	public JsonArray testSearch(){
		JsonArray searchResult = new JsonArray();

		try {
			Database database = new Database();
			Connection connection = database.Get_Connection();
			Project project = new Project();
			
			searchResult.addAll(project.searchTopMovies(connection, "", 0));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return searchResult;
	}
	
	public JsonArray search(String search, String type, int page) {
		JsonArray searchResult = new JsonArray();

		try {
			Database database = new Database();
			Connection connection = database.Get_Connection();
			Project project = new Project();

			switch (type) {

			case (MOVIE):
				searchResult.addAll(project.searchTopMovies(connection, search, page));
				break;
			case (GAME):
				searchResult.addAll(project.searchTopGames(connection, search, page));
				break;
			case (SERIES):
				searchResult.addAll(project.searchTopSeries(connection, search, page));
				break;
			default:
				break;
			}
		}

		catch (Exception e) {
			System.out.println("Exception Error"); // Console
		}
		return searchResult;
	}
}