package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Project {

	private final String TITLE = "Title";
	private final String POSTER = "Poster";
	private final String TYPE = "Type";
	private final String IMDBVOTES = "ImdbVotes";
	private final String IMDBRATING = "imdbRating";

	public JsonArray searchTopMovies(Connection connection, String title, int page) throws Exception {
		JsonArray searchData = new JsonArray();

		try {

			PreparedStatement ps = connection.prepareStatement(
					"SELECT Title, Poster, Type, imdbVotes, imdbRating FROM mydb.topmovies WHERE Title LIKE '%" + title
							+ "%' ORDER BY imdbVotes desc, imdbRating desc limit " + (page * 10) + ", 10");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				final JsonObject result = new JsonObject();

				String poster = rs.getString(POSTER);
				if (rs.wasNull()) {
					poster = "N/A";
				}
				
				result.addProperty(TITLE, rs.getString(TITLE));
				result.addProperty(POSTER, poster);
				result.addProperty(TYPE, rs.getString(TYPE));
				result.addProperty(IMDBRATING, String.valueOf(rs.getInt(IMDBRATING)));
				result.addProperty(IMDBVOTES, String.valueOf(rs.getInt(IMDBVOTES)));

				searchData.add(result);
			}
			return searchData;
		} catch (Exception e) {
			throw e;
		}
	}

	public JsonArray searchTopGames(Connection connection, String title, int page) throws Exception {
		JsonArray searchData = new JsonArray();
		try {
			PreparedStatement ps = connection.prepareStatement(
					"SELECT Title, Poster, Type, imdbVotes, imdbRating FROM mydb.topgames WHERE Title LIKE '%" + title
							+ "%' ORDER BY imdbVotes desc, imdbRating desc limit " + (page * 10) + ", 10");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				final JsonObject result = new JsonObject();

				String poster = rs.getString("Poster");
				if (rs.wasNull()) {
					poster = "N/A";
				}

				result.addProperty(TITLE, rs.getString(TITLE));
				result.addProperty(POSTER, poster);
				result.addProperty(TYPE, rs.getString(TYPE));
				result.addProperty(IMDBRATING, String.valueOf(rs.getInt(IMDBRATING)));
				result.addProperty(IMDBVOTES, String.valueOf(rs.getInt(IMDBVOTES)));

				searchData.add(result);
			}
			return searchData;
		} catch (Exception e) {
			throw e;
		}
	}

	public JsonArray searchTopSeries(Connection connection, String title, int page) throws Exception {
		JsonArray searchData = new JsonArray();
		try {
			PreparedStatement ps = connection.prepareStatement(
					"SELECT Title, Poster, Type,imdbVotes, imdbRating FROM mydb.topseries WHERE Title LIKE '%" + title
							+ "%' ORDER BY imdbVotes desc, imdbRating desc limit " + (page * 10) + ", 10");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				final JsonObject result = new JsonObject();

				String poster = rs.getString("Poster");
				if (rs.wasNull()) {
					poster = "N/A";
				}

				result.addProperty(TITLE, rs.getString(TITLE));
				result.addProperty(POSTER, poster);
				result.addProperty(TYPE, rs.getString(TYPE));
				result.addProperty(IMDBRATING, String.valueOf(rs.getInt(IMDBRATING)));
				result.addProperty(IMDBVOTES, String.valueOf(rs.getInt(IMDBVOTES)));

				searchData.add(result);
			}
			return searchData;
		} catch (Exception e) {
			throw e;
		}
	}
}